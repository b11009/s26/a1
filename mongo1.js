db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group:
		{
		_id: "$supplier_id", 
		avgStocksOnSale: {$avg:"$stocks"}
		}
	}
]);

