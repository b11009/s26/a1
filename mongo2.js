db.fruits.aggregate([
	{$unwind: "$origin"},
	{$match: 
		{$and:
			[{onSale: true},
			{origin: "Philippines"}]
		},
	},
	{$group:
		{
			_id: "$origin",
			maxPriceOfFruitsOnSaleInPhilippines: {$max: "$price"}
		}

	}
]);